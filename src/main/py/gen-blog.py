#!/usr/bin/env python3

import argparse
import functools
import glob
import os
import os.path

from skryba.index import debug, info, warning
from skryba.index import copytree, empty_fs, force_overwrite, from_glob, make_snippet_css, skryba_version, string2id, verbose
from skryba.post import make_post
from skryba.tag import Tag
from skryba.utils.collection import filter_dict_values_not_none

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog='gen-blog.py',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='''
Generates the complete blog by processing Jinja2 HTML template input files (--html) and post XML input files (--post).
Each post or tag will result in a corresponding HTML output file.

Contents of input-dir will be copied as is to the output directory.

i18n processing mode
--------------------

Unless --enable-i18n has been specified, Jinja2 HTML template files named post.html and tag.html must be present (--extra-tpl), unless there are no posts (or, respectively, tags). In this mode, tags will end up in output-dir/tag/ .
If --enable-i18n has been specified, then Skryba will search for post-LANG.html and tag-LANG.html, with LANG corresponding in each case to /post/@lang attribute found in post XML. In this mode, tags will end up in output-dir/LANG/tag/ .
''',
        add_help=True, allow_abbrev=False, epilog="""This program comes with ABSOLUTELY NO WARRANTY.""")

    parser.add_argument('--version', action='version', version='skryba {}'.format(skryba_version))
    parser.add_argument("--verbose", required=False, action="store_true", help="verbose processing")
    parser.add_argument("--overwrite-all", required=False, action="store_true", help="overwrite all files without prompting")
    parser.add_argument("--html", required=True, metavar="DIR", help="HTML template input directory")
    parser.add_argument("--post", required=False, metavar="DIR", help="XML post input directory")
    parser.add_argument("--extra-tpl", required=False, metavar="DIR", help="Extra templates directory. These files will not be used to generate HTML, but they will be made available in Jinja2 environment. For example: macros, base templates, post.html, tag.html, tag-en.html ...")
    parser.add_argument("--enable-i18n", required=False, action="store_true", help="enable i18n processing mode")
    parser.add_argument("--generate-css", required=False, metavar="DIR", help="Generate minimalistic, default CSS for HTML output for high-level elements (chatgpt, photo-gallery, snippet...). This should be a dir name, relative to output-dir. The CSS file to include will be: skryba.css")
    parser.add_argument("--generate-js", required=False, metavar="DIR", help="Generate minimalistic JavaScript code for HTML output for high-level elements (chatgpt, photo-gallery, snippet...). This should be a dir name, relative to output-dir. The JS file to include will be: skryba.js. Requires jQuery.")
    parser.add_argument(metavar="input-dir",  dest="indir",  help="Input directory with static files: images, CSS... These will be copied as is to the output directory.")
    parser.add_argument(metavar="output-dir", dest="outdir", help="output directory")
    args = parser.parse_args()

    if (args.verbose):
        verbose(True)
    if (args.overwrite_all):
        force_overwrite()

    # get the directory of this script
    this_dir = os.path.dirname(os.path.realpath(__file__))
    xslt_dir = os.path.join(this_dir, os.pardir, 'xslt')
    xpost    = os.path.join(xslt_dir, 'post.xslt')

    # Parse all posts using **/*.xml glob pattern
    #
    # [Post]
    #
    # TODO: since Python 3.11 it has: include_hidden=True, but we are at Python 3.9.
    #       https://docs.python.org/3/library/glob.html#glob.glob
    posts = from_glob(glob.glob(os.path.join(args.post, '**/*.xml'), recursive=True)) if (args.post) else empty_fs()
    debug("posts raw: {}".format(posts))
    posts = posts.filter_xml().map(functools.partial(make_post, xpost, os.path.abspath(args.post) if (args.post) else None))

    # post languages
    langs = None
    if args.enable_i18n:
        langs = set(map(lambda p: p.lang, posts))
        info("Found the following languages in posts: {}".format(langs))
        if (None in langs):
            warning("missing /post/@lang attribute in at least one post")
            langs = filter(bool, langs)
    debug('langs = {}'.format(langs))

    # generate the list of all tags
    #
    # reverse_dict         :  [Post]            -> tag, [Post]
    # map_values_with_keys :  tag, [Post]       -> tag, Tag
    # map_keys_with_values :  tag, Tag          -> Tag.filename, Tag (group and merge Tags by Tag filename)
    # values               :  Tag.filename, Tag -> [Tag]
    tags = posts.reverse_dict(lambda pi : pi.tags)     \
                .map_values_with_keys(
                    lambda t, pis: Tag(filename=(string2id(t) or '-'), value=t, posts=pis))       \
                .map_keys_with_values(                  \
                    lambda t, tag : tag.filename,       \
                    lambda y, z : Tag(filename=y.filename, value=y.value, posts=y.posts+z.posts, langs=y.langs|z.langs)) \
                .values()

    tags_by_lang = tags.reverse_dict(lambda t: t.langs)
    debug('tags_by_lang: {}'.format(tags_by_lang))

    # Fill in tag_objs field in posts
    for t in tags:
        for p in t.posts:
            p.append_tag(t)

    # copy the static files, including skryba.css and skryba.js
    copytree(args.indir, args.outdir, exclude=(filter(bool, [args.html, xslt_dir, args.post, args.extra_tpl, args.outdir])))
    if args.generate_css:
        copytree(os.path.join(this_dir, os.pardir, 'css'), os.path.join(args.outdir, args.generate_css))
    if args.generate_js:
        copytree(os.path.join(this_dir, os.pardir, 'js'), os.path.join(args.outdir, args.generate_js))

    # i18n mode: posts in different languages must be rendered using different post-*.html templates
    if args.enable_i18n:
        posts_gen = [(lang,
                      posts
                      .filter(lambda p: p.lang==lang)
                      .with_rendering_engine(args.extra_tpl)
                      .with_template('post-{}.html'.format(lang))) for lang in langs] # TODO: this filename interpolation is potentially unsafe
    else:
        posts_gen = [(None,posts.with_rendering_engine(args.extra_tpl).with_template('post.html'))]

    # generate the actual post HTML files (post.html or post-*.html)
    code_css = make_snippet_css()
    for ll,pg in posts_gen:
        info("Render posts for language ({})".format(ll))
        pg.render_all(
            lambda pi : pi.relpath,
            lambda pi : filter_dict_values_not_none({
                'lang'         : pi.lang,
                'path_to_root' : pi.rootpath,
                'post'         : pi,
                'posts_all'    : posts.all(),
                'snippet_css'  : code_css if pi.has_snippet else None,
                'tags'         : pi.tag_objs,
                'tags_all'     : tags.all(),
                'tags_by_lang' : tags_by_lang.all(),
            })
        ).copy_to(args.outdir)

    # i18n mode: tags in different languages must be rendered using different tag-*.html templates
    if args.enable_i18n:
        tags_gen = [(lang,
                     tags
                     .filter(lambda t: t.has_lang(lang))
                     .with_rendering_engine(args.extra_tpl)
                     .with_template('tag-{}.html'.format(lang))) for lang in langs] # TODO: this filename interpolation is potentially unsafe
    else:
        tags_gen = [(None,tags.with_rendering_engine(args.extra_tpl).with_template('tag.html'))]

    # generate the actual tag HTML files (tag.html or tag-*.html)
    for ll,tg in tags_gen:
        info("Render tags for language ({})".format(ll))
        tg.render_all(
            lambda t : '{}/tag/{}/index.html'.format(ll,t.filename) if (ll) else 'tag/{}/index.html'.format(t.filename),
            lambda t : {
                'path_to_root' : '../../..' if (ll) else '../..',
                'tag'          : t,
                'posts_all'    : posts.all(),
                'tags_all'     : tags.all(),
                'tags_by_lang' : tags_by_lang.all(),
            }
        ).copy_to(args.outdir)

    # Render all HTML templates using **/*.html glob pattern
    info("Render all templates ...")
    # TODO: since Python 3.11 it has: include_hidden=True, but we are at Python 3.9.
    #       https://docs.python.org/3/library/glob.html#glob.glob
    html_templates = from_glob(glob.glob(os.path.join(args.html, '**/*.html'), recursive=True))
    debug("html_templates: {}".format(html_templates))
    html_templates                                                        \
        .with_rendering_engine(
            [args.html, args.extra_tpl], include_skryba=True)             \
        .render_all_templates(
            args.html,
            posts_all=posts.all(),
            tags_all=tags.all(),
            tags_by_lang=tags_by_lang.all())                              \
        .copy_to(args.outdir)
