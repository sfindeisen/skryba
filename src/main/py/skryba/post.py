import datetime
import os.path
import re

from index import debug, info, warning
from index import normalize_string
from utils.collection import update_dict_if_absent

re_date = re.compile('^([0-9]{4})-([0-9]{2})-([0-9]{2})(?:;(.*))?$')
date_format_default = '%a, %-d %b %Y'

def _parse_date(pi):
    m = re_date.match(pi.origdate)
    if (m):
        g        = m.groups()
        pi.year  = g[0]
        pi.month = g[1]
        pi.day   = g[2]
        if (4 <= len(g)):
            pi.date_cmt = g[3]
        pi.date = datetime.date(int(pi.year), int(pi.month), int(pi.day))
    else:
        warning("Invalid date format: {}".format(pi.origdate))

def make_post(xpost, post_dir, skryba, filename, date_fmt=date_format_default, **kwargs):
    """Parses post XML file. Returns Post instance."""
    info("Process post: " + filename)

    pi = Post()
    pi.origpath = os.path.abspath(filename)
    pi.basename = os.path.basename(filename)[:-4]    # basename without .xml
    pi.relpath  = os.path.join(os.path.relpath(os.path.dirname(filename), start=post_dir), pi.basename, 'index.html')
    debug("relpath: {}".format(pi.relpath))
    pi.rellink  = os.path.join(os.path.relpath(os.path.dirname(filename), start=post_dir), pi.basename, '')
    debug("rellink: {}".format(pi.rellink))
    pi.rootpath = os.path.join(os.path.relpath(post_dir, start=os.path.dirname(os.path.join(filename, pi.basename))), '')
    debug("rootpath: {}".format(pi.rootpath))
    pi.title = skryba.xpath1('/post/title/text()')
    debug("title: {}".format(pi.title))
    pi.lang = skryba.xpath1('/post/@lang')
    debug("lang: {}".format(pi.lang))
    pi.tags = list(filter(bool, map(lambda s : normalize_string(s.strip()), skryba.xpath1('/post/tags/text()').split(';'))))
    debug("tags: {}".format(pi.tags))

    pi.origdate = skryba.xpath1('/post/@orig-date')
    debug("orig date: {}".format(pi.origdate))
    _parse_date(pi)
    if (pi.date):
        pi.date_fmt = pi.date.strftime(date_fmt)
        debug('date_fmt: {}'.format(pi.date_fmt))
    else:
        warning('unable to format post creation date; lang={} date={}'.format(pi.lang, pi.date))

    # pathToRoot is an XSLT parameter: https://lxml.de/xpathxslt.html#stylesheet-parameters
    # it must end with a slash (/) or be empty
    tpl_args = update_dict_if_absent(kwargs, pathToRoot="'{}'".format(pi.rootpath))
    pi.html = skryba.xslt_transform_to_html(xpost, **tpl_args)
    pi.has_snippet = (1 <= round(skryba.xpath('count(/post/body//snippet)')))
    return pi

class Post:
    """A single blog post."""
    def __init__(self):
        self.origpath     = None    # original input file path (absolute)
        self.relpath      = None    # output file path (relative to document root) (e.g.: blog/20230401-the-ultimate-email-client-setup/index.html)
        self.rellink      = None    # output file link (relative to document root) (e.g.: blog/20230401-the-ultimate-email-client-setup/)
        self.rootpath     = None    # relative path from the post output file to document root
        self.basename     = None    # input file base name
        self.title        = None    # post/title

        self.origdate     = None    # orig-date post attribute value
        self.year         = None
        self.month        = None
        self.day          = None
        self.date_cmt     = None
        self.date_fmt     = None    # formatted date (string)
        self.date         = None    # datetime.date object

        self.lang         = None    # language code: en, pl ...
        self.html         = None    # HTML of the post body
        self.tags         = None    # list of tags (string)
        self.tag_objs     = []      # list of Tag objects
        self.has_snippet  = False   # whether this post contains a <snippet> element (source code highlighting)

    def append_tag(self, tag):
        self.tag_objs.append(tag)

    def __repr__(self):
        clazz = self.__class__.__name__
        return '{}(origpath={}, lang={}, tags={})'.format(
            repr(clazz), repr(self.origpath), repr(self.lang), repr(self.tags))

    def __str__(self):
        return 'Post: lang={} basename={}'.format(self.lang, self.basename)
