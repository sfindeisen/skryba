def filter_dict(kv, p):
    """Given a dictionary, returns a new one retaining only pairs satisfying the predicate."""
    return { k:v for k,v in kv.items() if p(k,v) }

def filter_dict_values_not_none(kv):
    """Given a dictionary, returns a new one retaining only pairs where value is not None."""
    return filter_dict(kv, lambda k, v : (v is not None))

def update_dict_if_absent(kv, **kwargs):
    """Given a dictionary and a key-value mapping, returns a union of the two with mappings in the dictionary taking precedence."""
    d = dict(**kwargs)
    d.update(kv)
    return d
