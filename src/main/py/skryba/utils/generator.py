# The functions below are made available to the jinja environment, see skryba/generator.py .

def raise_exception(msg):
    raise Exception(msg)
