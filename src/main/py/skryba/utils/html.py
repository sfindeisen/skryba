def close_empty_tags(html_string):
    self_closing_tags = ['br', 'source']
    for t in self_closing_tags:
        html_string = html_string.replace('></{}>'.format(t), '/>')
    return html_string
