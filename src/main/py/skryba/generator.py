import collections.abc
import os
import os.path
import tempfile

import jinja2
import lxml.etree as ET

import base
from utils.collection import update_dict_if_absent
from utils.file import copyfile, copytree, write_file
from utils.log import debug
import utils.generator
import utils.text

class Generator(base.Base):
    """Basic output file generator."""
    def __init__(self, parent):
        super().__init__(parent)
        self.output_dir = tempfile.TemporaryDirectory()
        debug('Using temp outdir: {}'.format(self.output_dir.name))

    def all(self):
        """Returns all the items in this collection."""
        return self.parent.all()

    def output_filename(self, basename):
        return os.path.join(self.output_dir.name, basename)

    def copy_to(self, dest_dir):
        copytree(self.output_dir.name, dest_dir)
        return self

    def generate_all(self, f_name, f_contents):
        """
        Given a function f_name : item -> string and f_contents : item -> bytestring, calls them for each item
        to compute the output file name, the output file contents, and to write it out.
        """
        for x in self.all():
            write_file(self.output_filename(f_name(x)), (f_contents(x)))
        return self

    def generate_all_copy(self, f_src, f_dst):
        """
        Given two functions, f_src : item -> string and f_dst : item -> string, calls them for each item
        to compute the source and the destination file names, and to copy the source to the destination.
        """
        for x in self.all():
            copyfile((f_src(x)), self.output_filename(f_dst(x)))
        return self

class XMLGenerator(Generator):
    """Generator of XML files"""
    def __init__(self, parent):
        super().__init__(parent)

    def generate_xml_all(self, f_name, f_gen_xml_dom):
        """
        Given a function f_name : item -> string and f_gen_xml_dom : item -> XML DOM, calls them for each item
        to compute the output file name, the XML DOM and to write out the output file.
        """
        return self.generate_all(
            f_name,
            lambda x : ET.tostring(
                f_gen_xml_dom(x),
                pretty_print=True,
                xml_declaration=True,
                encoding='utf-8',
                method='xml'
        ))

class RenderingEngine(Generator):
    """
    jinja2 rendering interface using jinja2's FileSystemLoader and bound to a specific
    filesystem template search path.
    """

    def __init__(self, parent, search_path, include_skryba=True, template_file=None):
        """
        Set include_skryba parameter to true to have Skryba's own macro/template directory included in the
        Jinja2 search path.
        """
        super().__init__(parent)
        self.search_path = [os.path.abspath(search_path)] if isinstance(search_path, str) else list(map(os.path.abspath, search_path))
        self.include_skryba = include_skryba

        if self.include_skryba:
            extras_dir = RenderingEngine.jinja2_skryba_extras_dir()
            if extras_dir not in self.search_path:
                self.search_path.append(extras_dir)

        debug('jinja2 template search path: {}'.format(self.search_path))
        loader = jinja2.FileSystemLoader(searchpath=self.search_path, encoding='utf-8', followlinks=True)
        self.env = RenderingEngine.make_env(loader)
        debug('template_file: {}'.format(template_file))
        self.template_file = template_file

    @staticmethod
    def jinja2_skryba_extras_dir():
        this_dir   = os.path.dirname(os.path.realpath(__file__))
        extras_dir = os.path.join(this_dir, os.pardir, os.pardir, 'jinja')
        return os.path.abspath(extras_dir)

    @staticmethod
    def make_env(loader):
        # https://jinja.palletsprojects.com/en/3.1.x/api/#jinja2.Environment
        env = jinja2.Environment(loader=loader, undefined=jinja2.StrictUndefined, autoescape=True, auto_reload=False)
        # These vars are always available, see: https://jinja.palletsprojects.com/en/2.11.x/api/#jinja2.Environment.globals
        # Here we use them to insert some helper functions to the template environment.
        env.globals['skryba_raise']     = utils.generator.raise_exception
        return env

    def with_template(self, filename):
        return RenderingEngine(self, self.search_path, include_skryba=self.include_skryba, template_file=filename)

    def render_all(self, f_name, f_args):
        """
        Given a function f_name : item -> string and f_args : item -> dict, calls them for each item
        to compute the output file name (relative path), actual template parameters and to render
        the output file contents. If the underlying collection is a dictionary, item will be a key
        value pair.
        """
        all_items = self.all()

        if isinstance(all_items, collections.abc.Mapping):
            # the underlying collection is a dictionary (Mapping)
            for k, v in all_items.items():
                self.render((f_name((k,v))), **(f_args((k,v))))
        else:
            for x in all_items:
                output_file = f_name(x)
                tpl_args = update_dict_if_absent(f_args(x), pagename=output_file)
                self.render(output_file, **tpl_args)

        return self

    def render_all_templates(self, tpl_root, **kwargs):
        """
        Render all items as Jinja templates.

        tpl_root is a template directory root, which will be used as an offset to compute each
                 template's relative path. path_to_root will also be computed and passed on to
                 the template (if not already present).
        """
        tpl_root = os.path.abspath(tpl_root)
        debug('render_all_templates: root={}'.format(tpl_root))

        for item in self.all():
            debug('render_all_templates: {}'.format(item))
            itemrrel = os.path.relpath(item, start=tpl_root)
            template = self.env.get_template(itemrrel)
            debug('render_all_templates: name={}'.format(template.name))
            rootirel = os.path.relpath(tpl_root, start=os.path.dirname(item))
            debug('render_all_templates: path_to_root={}'.format(rootirel))

            # Here we pass 4 extra parameters to the Jinja template:
            #   - name         : the loading name of the template (base file name)
            #   - filename     : the filepath of the template in the filesystem
            #   - pagename     : template path relative to root (if not present)
            #   - path_to_root : root path relative to template (if not present)
            tpl_args = update_dict_if_absent(dict(**kwargs), pagename=itemrrel, path_to_root=rootirel)
            html = template.render(
                name=template.name,
                filename=template.filename,
                **tpl_args)
            write_file(self.output_filename(itemrrel), html.encode(encoding='utf-8', errors='strict'))
        return self

    def render(self, output_file, **kwargs):
        template = self.env.get_template(self.template_file)
        html = template.render(**kwargs)
        write_file(self.output_filename(output_file), html.encode(encoding='utf-8', errors='strict'))
        return self
