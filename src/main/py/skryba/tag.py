from index import debug

def _make_langs(posts):
    langs = set(filter(bool, map(lambda p: p.lang, posts)))
    debug("_make_langs: {} -> {}".format(posts, langs))
    return langs

class Tag:
    """A single article tag. Related posts can be in multiple different languages."""
    def __init__(self, filename=None, value=None, posts=[], langs=set()):
        self.filename = filename    # filename (base)
        self.value    = value       # Unicode (normalized)
        self.posts    = posts       # list of Post

        # set of language codes: en, pl ... (taken from posts)
        self.langs    = langs or _make_langs(self.posts)

    def has_lang(self, lang):
        return (lang in self.langs)

    def __repr__(self):
        return 'Tag(filename={}, value={})'.format(
            repr(self.filename), repr(self.value))

    def __str__(self):
        return 'Tag: filename={} value={} langs={}'.format(self.filename, self.value, self.langs)
