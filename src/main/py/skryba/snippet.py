import lxml.etree
import lxml.html

import pygments
import pygments.formatters
import pygments.lexers
import pygments.util

from utils.log import debug, warning

pygments_classprefix = 'skryba-snippet-pygments-'
pygments_cssclass    = 'skryba-snippet-pygments'

def _render_attrs(attrs):
    return ' '.join(map(lambda kv: '{}="{}"'.format(kv[0],kv[1]), attrs))
def _render_elem_open(elem):
    attrs = elem.items()
    return "<{} {}>".format(elem.tag, _render_attrs(attrs)) if attrs else "<{}>".format(elem.tag)
def _render_elem_close(elem):
    return "</{}>".format(elem.tag)

# elem parameter comes from lxml and will most likely be
# lxml.etree._ReadOnlyElementProxy .
def _extract_markup(elem, level=0):
    children   = list(elem)
    elem_open  = _render_elem_open(elem)  if (1 <= level) else ''
    elem_close = _render_elem_close(elem) if (1 <= level) else ''
    contents   = ''.join(map(lambda c : _extract_markup(c,1+level) + (c.tail or ''), children))
    return elem_open + (elem.text or '') + contents + elem_close

def _make_formatter(filename=None):
    # https://pygments.org/docs/formatters/#HtmlFormatter
    formopts = {
        'classprefix'        : pygments_classprefix,
        'cssclass'           : pygments_cssclass,
        'debug_token_types'  : False,
        'full'               : False,
        'linenos'            : 'table',
        'noclasses'          : False,
        'nowrap'             : False,
        'wrapcode'           : True,
    }

    if filename:
        formopts['filename'] = filename
        formopts['title']    = filename

    debug("formatter opts: {}".format(formopts))
    formatter = pygments.formatters.HtmlFormatter(**formopts)
    debug("formatter: {}".format(formatter));
    return formatter

def __make_lexer(filename, syntax, lexeropts):
    if syntax:
        return pygments.lexers.get_lexer_by_name(syntax, **lexeropts)
    if filename:
        try:
            return pygments.lexers.get_lexer_for_filename(filename, **lexeropts)
        except pygments.util.ClassNotFound as ex:
            warning("No lexer found: {}".format(ex))
    return None

def _make_lexer(filename=None, syntax=None, contents=None):
    # https://pygments.org/docs/lexers/
    lexeropts = {'stripnl': True, 'stripall': False}
    debug("lexer opts: {}".format(lexeropts))
    guess_lexer = pygments.lexers.guess_lexer(contents)
    debug("guess_lexer: {}".format(guess_lexer));
    lexer = __make_lexer(filename, syntax, lexeropts) or guess_lexer
    debug("lexer: {}".format(lexer));

    if (guess_lexer.name != lexer.name):
        warning("Using lexer ({}) but this source code looks more like ({})".format(lexer.name, guess_lexer.name))

    return lexer

def make_snippet_css():
    return _make_formatter().get_style_defs()

# XSLT extension element callback, dialed in from XSLT processor.
#
# https://lxml.de/extensions.html#xslt-extension-elements
class SnippetElement(lxml.etree.XSLTExtension):

    # The arguments passed to the .execute() method are:
    #
    # context
    #   The opaque evaluation context. You need this when calling back into the XSLT processor.
    # self_node
    #   A read-only Element object that represents the extension element in the stylesheet.
    # input_node
    #   The current context Element in the input document (also read-only).
    # output_parent
    #   The current insertion point in the output document. You can append elements or set the
    #   text value (not the tail). Apart from that, the Element is read-only.
    #
    # https://lxml.de/extensions.html#xslt-extension-elements
    # https://bugs.launchpad.net/lxml/+bug/2020895
    def execute(self, context, self_node, input_node, output_parent):
        filename = input_node.get("filename")
        debug("filename: {}".format(filename));
        syntax = input_node.get("syntax")
        debug("syntax: {}".format(syntax));

        node_contents = _extract_markup(input_node)
        # debug("node_contents: {}".format(node_contents));

        lexer = _make_lexer(filename=filename, syntax=syntax, contents=node_contents)
        formatter = _make_formatter(filename=filename)
        snippethi = pygments.highlight(node_contents, lexer, formatter)
        # debug('snippethi: {}'.format(snippethi))
        wrapper = lxml.html.fromstring(snippethi)
        output_parent.append(wrapper)
