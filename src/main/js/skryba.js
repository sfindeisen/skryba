function resize_blog_post_table_row() {
  const rowh = $(this).height();
  $(this).children("td").first().children("a").each(function() {
    $(this).outerHeight(rowh);
  });
};

function resize_blog_post_table() {
  $("table.skryba-post-table").filter(function() {
    return $(this).css("display") === "table"
  }).each(function() {
    $(this).children("tbody").children("tr").each(resize_blog_post_table_row);
    $(this).children("tr").each(resize_blog_post_table_row);
  });
};

/**
 * A snippet table can be too wide, for example if there are long code lines. This
 * function makes those narrower (or wider, if the window gets wide again).
 *
 * The snippet table:
 *
 * +-----------------------------+
 * | td.filename                 |
 * +--------------+--------------+
 * | td.linenos   | td.code      |
 * +--------------+--------------+
 */
function resize_pygments_snippets() {
  $("div.skryba-snippet-pygments").each(function() {
    // width excluding padding+border+margin. This is how much horizontal space we have.
    //
    // Note that .width() will always return the content width, regardless of the value of the CSS box-sizing property:
    // https://api.jquery.com/width/
    const wrapper_width = $(this).width();

    $(this).find("table.skryba-snippet-pygmentstable").filter(function() {
      return $(this).css("display") === "table"
    }).each(function() {
      const table_width_outer = $(this).outerWidth(true);
      const table_width       = $(this).width();
      const wdiff = wrapper_width - table_width_outer;
      // max allowed table width (excluding padding+border+margin)
      const max_table_width = Math.max(0, table_width + wdiff);

      // Line number column width (including padding+border+margin). This cannot be shrunk, or else
      // the line numbers would not be visible anymore.
      const linenos_width = $(this).find("td.linenos").first().outerWidth(true) ?? 0;

      // Now we are in one of two cases (reflected below):
      //
      // if (wdiff < 0) {
      //   The snippet table is too wide! Let's narrow it by narrowing the internal code div
      //   (which is presumably too wide... which can happen with very long code lines).
      //   This holds: 0 <= max_table_width <= table_width
      // } else {
      //   The snippet table width can be extended!
      //   This holds: max_table_width = table_width + wdiff
      // }

      $(this).find("td.code").first().each(function() {
        $(this).addClass("skryba-js-width-resize");
        $(this).children("div").first().each(function() {
          const codediv_width_outer = $(this).outerWidth(true);
          const codediv_width       = $(this).width();

          // actual code width based on children widths
          const code_width = Math.max(0,
            $(this).children("pre").first().outerWidth(true),
            $(this).children("pre").children("span").first().outerWidth(true) +
            $(this).children("pre").children("code").first().outerWidth(true));

            // set the new width (including padding+border+margin)
            const new_width_outer = Math.max(0, ((wdiff < 0) ? 0 : codediv_width_outer),
                                             Math.min(code_width + codediv_width_outer - codediv_width,
                                                      max_table_width - linenos_width));
            $(this).outerWidth(new_width_outer, true);
        });
      });
    });
  });
};

$(document).ready(function() {
  resize_blog_post_table();
  resize_pygments_snippets();
});

$(window).resize(function() {
  resize_blog_post_table();
  resize_pygments_snippets();
});
