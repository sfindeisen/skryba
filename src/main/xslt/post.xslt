<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:skryba="https://skryba.org/xml/ns"
  extension-element-prefixes="skryba"
  version="1.0">

<!-- This must end with a slash (or be empty) -->
<xsl:param name="pathToRoot">/</xsl:param>

<xsl:output method="html" indent="no" omit-xml-declaration="yes" standalone="no" encoding="UTF-8"/>

<xsl:template match="/post">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="/post/body">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="chatgpt">
    <xsl:element name="div">
        <xsl:attribute name="class">skryba skryba-chatgpt</xsl:attribute>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="chatgpt/answer">
    <xsl:element name="div">
        <xsl:attribute name="class">skryba skryba-answer</xsl:attribute>
        <xsl:element name="div">
            <xsl:attribute name="class">skryba skryba-logo</xsl:attribute>
        </xsl:element>
        <xsl:element name="div">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="chatgpt/prompt">
    <xsl:element name="div">
        <xsl:attribute name="class">skryba skryba-prompt</xsl:attribute>
        <xsl:element name="div">
            <xsl:attribute name="class">skryba skryba-logo</xsl:attribute>
        </xsl:element>
        <xsl:element name="div">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="snippet">
    <xsl:element name="div">
        <xsl:attribute name="class">skryba skryba-snippet</xsl:attribute>
        <skryba:snippet/>
    </xsl:element>
</xsl:template>

<xsl:template match="photo-gallery">
    <xsl:element name="ol">
        <xsl:attribute name="class">skryba skryba-photo-gallery</xsl:attribute>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="photo-gallery/photo">
    <xsl:element name="li">
        <xsl:attribute name="class">skryba skryba-photo</xsl:attribute>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="audio">
    <audio>
        <xsl:copy-of select="@class | @controls | @preload"/>
        <xsl:apply-templates/>
    </audio>
</xsl:template>

<xsl:template match="audio/source">
    <source>
        <xsl:attribute name="src"><xsl:value-of select="$pathToRoot"/><xsl:value-of select="./@src"/></xsl:attribute>
        <xsl:copy-of select="@class | @type"/>
    </source>
</xsl:template>

<xsl:template match="br|code|del|dd|dl|dt|em|h1|h2|h3|h4|h5|h6|li|ol|p|pre|q|span|strong|ul">
    <xsl:element name="{local-name()}">
        <xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute></xsl:if>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="bible-quote">
    <xsl:variable name=       "all_verses" select="count(./verse)"/>
    <xsl:variable name=  "numbered_verses" select="count(./verse[@i])"/>
    <xsl:variable name="unnumbered_verses" select="count(./verse[not(@i)])"/>

    <xsl:if test="($numbered_verses != 0) and ($unnumbered_verses != 0)">
      <xsl:message terminate="no">Bible quote contains a mix of numbered and unnumbered verses: <xsl:value-of select="."/></xsl:message>
    </xsl:if>

    <xsl:element name="span">
      <xsl:attribute name="class"><xsl:if test="@class"><xsl:value-of select="@class"/><xsl:text> </xsl:text></xsl:if>skryba skryba-bible-quote</xsl:attribute>
      <xsl:element name="span">
          <xsl:attribute name="class">skryba skryba-verse-list</xsl:attribute>
          <xsl:apply-templates/>
      </xsl:element>
      <xsl:element name="span">
          <xsl:attribute name="class">skryba skryba-bible-ref</xsl:attribute>
          <xsl:element name="span">
              <xsl:attribute name="class">skryba skryba-book</xsl:attribute>
              <xsl:value-of select="./@book"/>
          </xsl:element>
          <xsl:element name="span">
              <xsl:attribute name="class">skryba skryba-range</xsl:attribute>
              <xsl:value-of select="./@range"/>
          </xsl:element>
          <xsl:element name="span">
              <xsl:attribute name="class">skryba skryba-edition</xsl:attribute>
              <xsl:value-of select="./@edition"/>
          </xsl:element>
      </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="verse">
    <xsl:element name="span">
        <xsl:attribute name="class">skryba skryba-verse</xsl:attribute>
        <xsl:if test="@i">
            <xsl:element name="span">
                <xsl:attribute name="class">skryba skryba-number</xsl:attribute>
                <xsl:value-of select="@i"/>
            </xsl:element>
        </xsl:if>
        <xsl:element name="span">
            <xsl:attribute name="class">skryba skryba-line</xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="a">
  <xsl:variable name="h" select="./@href"/>
  <xsl:variable name="z" select="string-length($h)"/>

  <a>
    <xsl:choose>
      <!-- TODO use regex instead -->
      <!-- TODO add more protocols -->
      <xsl:when test="starts-with($h, 'http://') or starts-with($h, 'https://') or starts-with($h, 'ftp://')">
        <!-- absolute url, render as is -->
        <xsl:attribute name="href"><xsl:value-of select="$h"/></xsl:attribute>
        <xsl:attribute name="target">_blank</xsl:attribute>
      </xsl:when>
      <xsl:when test="(4 &lt;= $z) and ('.xml' = substring($h, $z - 3))">
        <!-- cross-link to another post -->
        <xsl:attribute name="href"><xsl:value-of select="concat(substring($h, 1, $z - 3), 'html')"/></xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="no">Unknown link: <xsl:value-of select="$h"/></xsl:message>
        <xsl:attribute name="href"><xsl:value-of select="$h"/></xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>

    <xsl:apply-templates/>
  </a>
</xsl:template>

<!-- img can be part of photo-gallery , but does not need to be.
     In any case, src-thumb attribute will be supported.
  -->
<xsl:template match="img">
  <xsl:choose>
    <xsl:when test="@src-thumb">
      <a>
        <xsl:attribute name="class">skryba skryba-img-thumb</xsl:attribute>
        <xsl:attribute name="href"><xsl:value-of select="$pathToRoot"/><xsl:value-of select="./@src"/></xsl:attribute>
        <img>
          <xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute></xsl:if>
          <xsl:attribute name="alt"><xsl:value-of select="./@alt"/></xsl:attribute>
          <xsl:attribute name="src"><xsl:value-of select="$pathToRoot"/><xsl:value-of select="./@src-thumb"/></xsl:attribute>
        </img>
      </a>
    </xsl:when>
    <xsl:otherwise>
      <img>
        <xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute></xsl:if>
        <xsl:attribute name="alt"><xsl:value-of select="./@alt"/></xsl:attribute>
        <xsl:attribute name="src"><xsl:value-of select="$pathToRoot"/><xsl:value-of select="./@src"/></xsl:attribute>
      </img>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="youtube">
    <xsl:element name="iframe">
        <xsl:attribute name="class">skryba skryba-youtube</xsl:attribute>
        <xsl:attribute name="width">480</xsl:attribute>
        <xsl:attribute name="height">270</xsl:attribute>
        <xsl:attribute name="src"><xsl:text>https://www.youtube-nocookie.com/embed/</xsl:text><xsl:value-of select="./@v"></xsl:value-of></xsl:attribute>
        <xsl:attribute name="title">YouTube video player</xsl:attribute>
        <xsl:attribute name="allow">accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share</xsl:attribute>
        <xsl:attribute name="allowfullscreen">allowfullscreen</xsl:attribute>
        <xsl:attribute name="loading">eager</xsl:attribute>
    </xsl:element>
</xsl:template>

<!-- void template -->
<xsl:template match="tags|title"/>

<!-- catch-all template -->
<xsl:template match="*">
    <xsl:message terminate="no">Unmatched element: <xsl:value-of select="name()"/></xsl:message>
    <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>
