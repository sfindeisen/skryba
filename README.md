# Skryba --- static blog or page generator

```console
usage: gen-blog.py [-h] [--version] [--verbose] [--overwrite-all]
                   --html DIR [--post DIR] [--extra-tpl DIR]
                   [--enable-i18n] [--generate-css DIR]
                   [--generate-js DIR]
                   input-dir output-dir

Generates the complete blog by processing Jinja2 HTML template input files (--html) and post XML input files (--post).
Each post or tag will result in a corresponding HTML output file.

Contents of input-dir will be copied as is to the output directory.

i18n processing mode
--------------------

Unless --enable-i18n has been specified, Jinja2 HTML template files named post.html and tag.html must be present (--extra-tpl), unless there are no posts (or, respectively, tags). In this mode, tags will end up in output-dir/tag/ .
If --enable-i18n has been specified, then Skryba will search for post-LANG.html and tag-LANG.html, with LANG corresponding in each case to /post/@lang attribute found in post XML. In this mode, tags will end up in output-dir/LANG/tag/ .

positional arguments:
  input-dir           Input directory with static files: images,
                      CSS... These will be copied as is to the
                      output directory.
  output-dir          output directory

optional arguments:
  -h, --help          show this help message and exit
  --version           show program's version number and exit
  --verbose           verbose processing
  --overwrite-all     overwrite all files without prompting
  --html DIR          HTML template input directory
  --post DIR          XML post input directory
  --extra-tpl DIR     Extra templates directory. These files will
                      not be used to generate HTML, but they will
                      be made available in Jinja2 environment. For
                      example: macros, base templates, post.html,
                      tag.html, tag-en.html ...
  --enable-i18n       enable i18n processing mode
  --generate-css DIR  Generate minimalistic, default CSS for HTML
                      output for high-level elements (chatgpt,
                      photo-gallery, snippet...). This should be a
                      dir name, relative to output-dir. The CSS
                      file to include will be: skryba.css
  --generate-js DIR   Generate minimalistic JavaScript code for
                      HTML output for high-level elements (chatgpt,
                      photo-gallery, snippet...). This should be a
                      dir name, relative to output-dir. The JS file
                      to include will be: skryba.js. Requires
                      jQuery.

This program comes with ABSOLUTELY NO WARRANTY.
```

Focus on your content. Write your blog posts in a lightweight, HTML-friendly XML:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<post orig-date="1983-10-15; my birthday!" lang="en">
<title>My 1983 birthday</title>
<tags>birthday; presents</tags>
<body>
<p>Today is my birthday!</p>
<img alt="party" src="img/brueghel.png"/>
<p>I got <a href="ibm.xml">this</a>!</p>
</body>
</post>
```

Use powerful [Jinja2](http://jinja.pocoo.org/) templates to define your page layout. For example, here's how you could write your tag page:

```jinja
{% extends "index.html" %}
{% block title %}Tag: {{ tag }}{% endblock %}
{% block main %}
<h2>Posts for tag: {{ tag }}</h2>

<table class="post-table">
<tr><th>Title</th><th>Date</th><th>Language</th><th>Tags</th></tr>
{% for post in post_list %}
<tr>
<td><a href="./{{ path_to_root }}/post/{{ post.basename }}">{{ post.title }}</a></td>
<td>{{ post.origdate }}</td>
<td>{{ post.lang }}</td>
<td>
{% for tag in post.tags %}
{{ tag }}
{% endfor %}
</td>
</tr>
{% endfor %}
</table>
{% endblock %}
```

Here, `tag` and `post_list` are variables exported to your template from Skryba.

Use CSS to make it nice-looking.

## [Documentation](./docs/)

## TODO

- [x] get rid of string tags, use Tag class objects thoroughly instead
- [x] get rid of tag_list macro
- [x] batch mode aka overwrite all files without any questions
- [ ] multiple authors
- [ ] multiple blogs using different templates (e.g. post-en.html, project.html etc.)
- [ ] (?) multiple post directories (multiple blogs)
- [ ] language index (list of languages, list of posts per language)
- [ ] Bible quote index
- [x] highlight current post in the menu
- [x] post tags: make them links
- [ ] disquss
- [x] the list of restricted template names (`layout.html`, `macro.html`, `post.html`, `tag.html`, `tag-cloud.html`) is somewhat ad-hoc; make it cleaner (a subdir for user templates perhaps?...)
- [ ] chatgpt sessions: include the avatar of the human interviewer
- [ ] sitemap support
- [ ] easy linking from post to a tag (<tag-ref> or similar)
- [ ] auto target=blank support for links in HTML templates (not just in posts)
- [ ] check for broken (internal) links
